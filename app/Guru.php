<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table    = "m_guru";
    protected $fillable = [
        "nik",
        "nama", 
        "no_hp", 
        "tgl_lahir", 
        "alamat", 
        "user_id"
    ];

    // Relasi dengan Tabel User
    public function user() {
        return $this    ->  belongsTo('App\User', 'user_id');
    }
    public function contentMapel()
    {
        return $this->hasMany('App\ContentMapel');
    }
    public function mapel()
    {
        return $this->belongsTo('App\Mapel');
    }
    // public function user()
    // {
    //     return $this->hasOne('App\User', 'user_id');
    // }
}