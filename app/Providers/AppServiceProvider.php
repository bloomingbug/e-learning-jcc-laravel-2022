<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;

use Illuminate\Support\ServiceProvider;

use Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Gate::define('admin', function($user) {
            return Str::lower($user->role->nama) === 'admin';
        });
        Gate::define('guru', function($user) {
            return Str::lower($user->role->nama) === 'guru';
        });
        Gate::define('siswa', function($user) {
            return Str::lower($user->role->nama) === 'siswa';
        });
    }
}