<?php

namespace App\Http\Middleware;

use Auth;

use Str;

use Closure;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check() || Str::lower((Auth::user()->role->nama)) !== 'admin' ) {
            abort(403);
        }
        return $next($request);
    }
}