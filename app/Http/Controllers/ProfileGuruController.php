<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\Guru;

use Illuminate\Support\Facades\Auth;

use Str;

class ProfileGuruController extends Controller
{
    // Mengalihkan ke halaman profile guru
    public function index()
    {
        $profile    = Guru::where('user_id', Auth::id())
                        ->first();
        return view('pguru.edit', compact('profile'));
    }

    
    public function update(Request $request, $id)
    {
        $request->validate(
        [
            'nama_lengkap'  => 'required',
            'tgl_lahir'     => 'required',
            'alamat'        => 'required',
            'no_hp'         => 'required',
        ],
        [
        'nama_lengkap.required' => 'Kolom Nama Lengkap harus diisi yah kawan!',
        'tgl_lahir.required'    => 'Kolom Tanggal Lahir harus diisi yah kawan!',
        'alamat.required'       => 'Kolom Alamat harus diisi yah kawan!',
        'no_hp.required'        => 'Kolom Nomor Handphone harus diisi yah kawan!',
        ]);

        $siswa              = Siswa::findOrFail($id);
        $siswa->nama        = $request->nama_lengkap;
        $siswa->tgl_lahir   = $request->tgl_lahir;
        $siswa->alamat      = $request->alamat;
        $siswa->no_hp       = $request->no_hp;
        $siswa->save();

        return redirect('/dashboard');
    }
}