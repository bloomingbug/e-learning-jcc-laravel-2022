<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mapel;
use App\Guru;
use Alert;
use App\Http\Controllers\ContentMapelController;
use App\ContentMapel;
use Illuminate\Support\Facades\Auth;

class MapelController extends Controller
{
    public function index()
    {
        //memberikan hasil untuk menampilkan view index pada folder mapel
        $mapel      = Mapel::all();
        return view('mapel.index', compact('mapel'));
    }

    public function create()
    {
        //memberikan hasil untuk menampilkan view create pada folder mapel
        return view('mapel.create');
        
    }

    public function store(Request $request)
    {
        //validasi input dari view create.blade.php
        $request->validate(
            [
                'kode_mapel'                => 'required',
                'nama_mapel'                => 'required',
                'deskripsi_mapel'           => 'required',
            ],
        //custom output message error
            [            
                'kode_mapel.required'       => 'kolom Kode Mata Pelajaran harus diisi',
                'nama_mapel.required'       => 'kolom Nama Mata Pelajaran harus diisi',
                'deskripsi_mapel.required'  => 'kolom Deskripsi Mata Pelajaran harus diisi',
            ]
        );
        $mapel  = new Mapel;
        $mapel->kode_mapel          = $request->kode_mapel;
        $mapel->nama_mapel          = $request->nama_mapel;
        $mapel->deskripsi_mapel     = $request->deskripsi_mapel;
        $mapel->save();
        Alert::toast("Data Mata Pelajaran ". $mapel->nama_mapel." Berhasil dihapus", 'success');
        return redirect('/mapel');

    }

    public function show($id)
    {
        //memberikan hasil untuk menampilkan view show pada folder mapel
        $mapel      = Mapel::findOrFail($id);
        return view('mapel.show', compact('mapel'));
    }

    public function edit($id)
    {
        //memberikan hasil untuk menampilkan view edit pada folder mapel
        $mapel      = Mapel::findOrFail($id);
        return view('mapel.edit', compact('mapel'));
    }


    public function update(Request $request, $id)
    {
        //validasi input dari view edit.blade.php
        $request->validate(
            [
                'kode_mapel'                => 'required',
                'nama_mapel'                => 'required',
                'deskripsi_mapel'           => 'required',
            ],
        //custom output message error
            [            
                'kode_mapel.required'       => 'kolom Kode Mata Pelajaran harus diisi',
                'nama_mapel.required'       => 'kolom Nama Mata Pelajaran harus diisi',
                'deskripsi_mapel.required'  => 'kolom Deskripsi Mata Pelajaran harus diisi',
            ]
        );
        $mapel = Mapel::find($id);
        $mapel->kode_mapel          = $request->kode_mapel;
        $mapel->nama_mapel          = $request->nama_mapel;
        $mapel->deskripsi_mapel     = $request->deskripsi_mapel;
        Alert::toast("Data Mata Pelajaran ". $mapel->nama_mapel." Berhasil dihapus", 'success');
        $mapel->save();
        return redirect('/mapel');
    }

    public function destroy($id)
    {
        //memproses hapus pada tombol delete
        $mapel = Mapel::find($id);
        Alert::toast("Data Mata Pelajaran ". $mapel->nama_mapel." Berhasil dihapus", 'success');
        $mapel->delete();
        return redirect('/mapel');
    }
}