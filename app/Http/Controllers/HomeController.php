<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Str;

use App\User;
use App\Siswa;
use App\Guru;
use App\Mapel;
use App\Kelas;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {   
        $jumlah_user = User::count(); 
        $jumlah_guru = Guru::count(); 
        $jumlah_siswa = Siswa::count(); 
        $jumlah_mapel = Mapel::count();
        $mapel = Mapel::all();
        $kelas = Kelas::all(); 
        return view('index', compact('jumlah_user', 'jumlah_guru', 'jumlah_siswa', 'jumlah_mapel', 'kelas', 'mapel'));
    }
}