<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

use App\User;
use App\Guru;
use App\Role;
use App\Kelas;

class GuruController extends Controller
{
    // Menampilkan Daftar Guru
    public function index()
    {
        $users  = User::all();
        $guru   = Guru::all();
        $role   = Role::all();

        return view('guru.index', compact('users', 'guru', 'role'));
    }

    // Mengalihkan ke halaman create guru
    public function create()
    {   
        $role   = Role::all();
        $users  = User::all();
        return view('guru.create', compact('role', 'users'));
    }

    // Mengirim data ke DB
    // Nantinya m_user_id tidak diinput manual tapi dari Auth
    public function store(Request $request)
    {
        $request->validate(
        [
            'nik'               => 'required',
            'nama'              => 'required',
            'tgl_lahir'         => 'required',
            'alamat'            => 'required',
            'no_hp'             => 'required',
            'name'              => 'required|unique:m_users',
            'email'             => 'required|unique:m_users',
            'password'          => 'required|min:7;',
            'role_id'           => 'required',
        ],
        [
        'nik.required'          => 'Kolom NIS harus diisi yah kawan!',
        'nama.required'         => 'Kolom Nama Lengkap harus diisi yah kawan!',
        'tgl_lahir.required'    => 'Kolom Tanggal Lahir harus diisi yah kawan!',
        'alamat.required'       => 'Kolom Alamat harus diisi yah kawan!',
        'no_hp.required'        => 'Kolom Nomor Handphone harus diisi yah kawan!',
        'name.required'         => 'Kolom Username harus diisi yah kawan!',
        'name.unique'           => 'Username telah digunakan!',
        'email.required'        => 'Kolom Email harus diisi yah kawan!',
        'email.unique'          => 'Email telah digunakan!',
        'password.required'     => 'Kolom Password harus diisi yah kawan!',
        'password.min'          => 'Password minimal 8 karakter!',
        'role_id.required'      => 'Kolom Role harus diisi yah kawan!',
        ]);

        // Table User
        $arrayUser = [
            'name'          => Str::lower($request->name),
            'email'         => $request->email,
            'password'      => Hash::make($request->password),
            'role_id'       => $request->role_id
        ];
        $user   = DB::table('m_users')->insertGetId($arrayUser);

        // Table Guru
        $arrayGuru = [
            'nik'           => $request->nik,
            'nama'          => $request->nama,
            'tgl_lahir'     => $request->tgl_lahir,
            'alamat'        => $request->alamat,
            'no_hp'         => $request->no_hp,
            'user_id'       => $user
        ];
        $siswa  = DB::table('m_guru')->insertGetId($arrayGuru);
        
        return redirect('/guru');
    }

    // Menampilkan detail dari siswa yang dipilih
    public function show($id)
    {
        $guru       = Guru::findOrFail($id);
        return view('guru.show', compact('guru'));
    }

    // Mengalihkan ke halaman edit data
    public function edit($id)
    {
        $guru       = Guru::findOrFail($id);
        return view('guru.edit', compact('guru'));
    }

    // Mengirim data update ke DB
    public function update(Request $request, $id)
    {
        $request->validate(
        [
            'nama'      => 'required',
            'tgl_lahir' => 'required',
            'alamat'    => 'required',
            'no_hp'     => 'required',
        ],
        [
        'nama.required'         => 'Kolom Nama Lengkap harus diisi yah kawan!',
        'tgl_lahir.required'    => 'Kolom Tanggal Lahir harus diisi yah kawan!',
        'alamat.required'       => 'Kolom Alamat harus diisi yah kawan!',
        'no_hp.required'        => 'Kolom Nomor Handphone harus diisi yah kawan!',
        ]);

        $arrayGuru = [
            'nama'          => $request->nama,
            'tgl_lahir'     => $request->tgl_lahir,
            'alamat'        => $request->alamat,
            'no_hp'         => $request->no_hp
        ];
        $siswa  = DB::table('m_guru')->where('id', $id)->update($arrayGuru);

        DB::table('m_users')
                        ->where('id', $request->user_id)
                        ->update(['name' => $request->username, 'email' => $request->email]);
        
        return redirect('/guru');
    }

    // Menghapus data
    public function destroy($id)
    {
        $guru   =   Guru::findOrFail($id);
        $guru   ->  delete();
        return redirect('/guru');
    }
}