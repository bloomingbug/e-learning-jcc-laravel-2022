<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Panggil Model Kelas
use App\Kelas;
use Alert;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kelas = Kelas::all();
        return view('kelas.index', compact('kelas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //memberikan hasil untuk menampilkan view create pada folder kelas
        return view('kelas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validasi input dari view create.blade.php folder kelas
        $request->validate(
            [
                'kode_kelas'                => 'required',
                'nama_kelas'                => 'required',
            ],
        //custom output message error
            [            
                'kode_kelas.required'       => 'kolom Kode Kelas harus diisi',
                'nama_kelas.required'       => 'kolom Nama Kelas harus diisi',
            ]
        );
        //masukan data ke tabel kelas
        $kelas  = new Kelas;
        $kelas->kode_kelas          = $request->kode_kelas;
        $kelas->nama_kelas          = $request->nama_kelas;
        $kelas->save();
        Alert::toast("Data Kelas ". $kelas->nama_kelas." Berhasil Ditambahkan", 'success');
        return redirect('/kelas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //memberikan hasil untuk menampilkan view show pada folder kelas
        $kelas       = Kelas::find($id);
        return view('kelas.show', compact('kelas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kelas       = Kelas::find($id);
        return view('kelas.edit', compact('kelas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Untuk Validasi
        $request->validate(
            [   
                'kode_kelas'          => 'required',
                'nama_kelas'          => 'required',
            ],
            [
                'kode_kelas.required' => 'Kolom Kode Kelas harus diisi yah kawan!',
                'nama_kelas.required' => 'Kolom Nama Kelas harus diisi yah kawan!',
            ]);
    
            // Pengiriman
            $kelas                     = Kelas::findOrFail($id);
            $kelas->kode_kelas         = $request->kode_kelas;
            $kelas->nama_kelas         = $request->nama_kelas;
            $kelas->update();
            Alert::toast("Data ". $kelas->kode_kelas." Berhasil Diupdate", 'success');
            return redirect('/kelas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kelas = Kelas::findOrFail($id);
        Alert::toast("Kelas ". $kelas->kode_kelas." berhasil dihapus", 'success');
        $kelas->delete();
        return redirect('/kelas');
    }
}
