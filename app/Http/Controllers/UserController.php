<?php

namespace App\Http\Controllers;

use App\Guru;

use Illuminate\Http\Request;

use Illuminate\Support\Str;

use Illuminate\Support\Facades\Hash;

use Auth;

use App\User;

use App\Role;

use App\Siswa;

class UserController extends Controller
{
    // Menampilkan data users
    public function index()
    {    
        $this   ->  authorize('admin');
        $users  =   User::all();
        return view('user.index', compact('users'));
    }

    // Mengalihkan ke Halaman Create
    public function create()
    {
        $role   = Role::all();
        return view('user.create', compact('role'));
    }

    // Mengirim data ke DM
    // Password sudah di HASH
    public function store(Request $request)
    {
        // Untuk Validasi
        $request->validate(
        [
            'name'      => 'required',
            'email'     => 'required',
            'password'  => 'required',
            'role_id'   => 'required'
        ],
        [
        'name.required'     => 'Kolom nama harus diisi yah kawan!',
        'email.required'    => 'Kolom email harus diisi yah kawan!',
        'password.required' => 'Kolom password harus diisi yah kawan!',
        'role_id.required'  => 'Kolom Role harus diisi yah kawan!',
        ]);


        // Pengiriman data
        $user           = new User;

        $hash_password  = Hash::make($request->password);

        $user->name     = Str::lower();
        $user->email    = $request->email;
        $user->password = $hash_password;
        $user->role_id  = $request->role_id;
        toast('User '.$request->name.' telah ditambahkan!','success');
        $user->save();
        return redirect('/user');
        
    }

    //Detail masing masing user
    public function show($id)
    {   
        $user   = User::findOrFail($id);
        return view('user.show', compact('user'));
    }

    // Mengarahkan ke halaman edit
    public function edit($id)
    {   
        $role = Role::all();
        $user = User::findOrFail($id);
        return view('user.edit', compact('user', 'role'));
    }

    // Mengirim update data ke DV
    public function update(Request $request, $id)
    {
        // Untuk Validasi
        $request->validate(
        [
            'name'      => 'required',
            'email'     => 'required',
            'role_id'   => 'required'
        ],
        [
        'name.required'      => 'Kolom nama harus diisi yah kawan!',
        'email.required'     => 'Kolom email harus diisi yah kawan!',
        'role_id.required'   => 'Kolom Role harus diisi yah kawan!',
        ]);

        // Pengiriman data
        $user           = User::findOrFail($id);
        $user->name     = Str::lower($request->name);
        $user->email    = $request->email;
        $user->role_id  = $request->role_id;
        $user->update();
        return redirect('/user');
    }

    // Menghapus data user
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('/user');
    }
}