<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mapel;

class MapelSiswaController extends Controller
{
    //
    public function index()
    {
        $this->authorize('siswa');
        $mapel      = Mapel::all();
        return view('mapelsiswa.index', compact('mapel'));
    }
    public function show($id)
    {
        //memberikan hasil untuk menampilkan view show pada folder mapel
        $mapel      = Mapel::findOrFail($id);
        return view('mapelsiswa.show', compact('mapel'));
    }
}
