<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Exports\SiswaExport;

use App\Exports\GuruExport;

use App\Exports\KelasExport;

use App\Exports\MapelExport;

use Maatwebsite\Excel\Facades\Excel;

use DateTime;
use DateTimeZone;

class ExcelController extends Controller
{
    // Export data
    public function export_siswa() 
    {
        return Excel::download(new SiswaExport, 'Elearning - Data Siswa.xlsx');
    }
    
    public function export_guru() 
    {
        return Excel::download(new GuruExport, 'Elearning - Data Guru.xlsx');
    }
    
    public function export_kelas() 
    {
        return Excel::download(new KelasExport, 'Elearning - Data Kelas.xlsx');
    }
    
    public function export_mapel() 
    {
        return Excel::download(new MapelExport, 'Elearning - Data Mata Pelajaran.xlsx');
    }
}