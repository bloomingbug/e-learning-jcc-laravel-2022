<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Role;

use Alert;

use App\User;

class RoleController extends Controller
{
    // Menampilkan Daftar Role
    public function index()
    {   
        $role   = Role::all();
        return view('role.index', compact('role'));
    }

    // Mengalihkan ke Halaman Tambah Role
    public function create()
    {
        return view('role.create');
    }

    // Mengirim data Role ke DB
    public function store(Request $request)
    {   
        // Untuk Validasi
        $request->validate(
        [
            'nama'          => 'required'
        ],
        [
            'nama.required' => 'Kolom nama harus diisi yah kawan!'
        ]);

        // Pengiriman
        $role       = new Role;
        $role->nama = $request->nama;
        Alert::toast("Role ". $role->nama." berhasil ditambahkan", 'success');
        $role->save();
        return redirect('/role');
    }

    // Melihat data yang ada dalam masing-masing Role
    public function show($id)
    {
        $role       = Role::findOrFail($id);
        return view('role.show', compact('role'));
    }

    // Mengalihkan ke Halaman Edit
    public function edit($id)
    {
        $role       = Role::find($id);
        return view('role.edit', compact('role'));
    }

    // Mengirim data update ke DB
    public function update(Request $request, $id)
    {
        // Untuk Validasi
        $request->validate(
        [
            'nama'          => 'required'
        ],
        [
            'nama.required' => 'Kolom nama harus diisi yah kawan!'
        ]);

        // Pengiriman
        $role               = Role::findOrFail($id);
        $role->nama         = $request->nama;
        $role->update();
        return redirect('/role');
    }

    // Menghapus Data
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        Alert::toast("Role ". $role->nama." berhasil dihapus", 'success');
        $role->delete();
        return redirect('/role');
    }
}