<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\Siswa;

use Illuminate\Support\Facades\Auth;

use Str;

class ProfileSiswaController extends Controller
{
    // Mengalihkan ke halaman profile siswa
    public function index()
    {   
        $profile = Siswa::where('user_id', Auth::id())->first();
        // $profile = User::all();
        // dd($profile);
        return view('psiswa.edit', compact('profile'));
    }
    
    public function update(Request $request, $id)
    {
        $request->validate(
        [
            'nama_lengkap'  => 'required',
            'tgl_lahir'     => 'required',
            'alamat'        => 'required',
            'no_hp'         => 'required',
        ],
        [
        'nama_lengkap.required' => 'Kolom Nama Lengkap harus diisi yah kawan!',
        'tgl_lahir.required'    => 'Kolom Tanggal Lahir harus diisi yah kawan!',
        'alamat.required'       => 'Kolom Alamat harus diisi yah kawan!',
        'no_hp.required'        => 'Kolom Nomor Handphone harus diisi yah kawan!',
        ]);

        $siswa                  = Siswa::findOrFail($id);
        $siswa->nama_lengkap    = $request->nama_lengkap;
        $siswa->tgl_lahir       = $request->tgl_lahir;
        $siswa->alamat          = $request->alamat;
        $siswa->no_hp           = $request->no_hp;
        $siswa->save();

        return redirect('/');
    }
}