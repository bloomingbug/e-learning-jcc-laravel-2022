<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mapel;
use App\Guru;
use App\ContentMapel;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;

class ContentMapelController extends Controller
{
    //

    public function index()
    {
        $content_mapel  = ContentMapel::all();
        return view('content.index', compact('content_mapel'));
    }

    public function create(Request $request)
    {
        //memberikan hasil untuk menampilkan view create pada folder mapel
        $guru       = Guru::all();
        $mapel_id   = $request->segment(2);
        return view('content.create', compact('guru', 'mapel_id'));
        
    }
    public function store(Request $request)
    {
        $request->validate(
            [
                
                'nama_content_mapel'    => 'required',
                'file'                  => 'required|mimes:pdf|max:2200',
                'rangkuman'             => 'required',
                'date_open'             => 'required',
                'date_close'            => 'required',
                'mapel_id'              => 'required',
                'guru_id'               => 'required',
            ],
            [            
                'nama_content_mapel.required'  => 'kolom nama_content_mapel harus diisi',
                'file.required'                => 'kolom file harus diisi',
                'file.mimes'                   => 'file harus berekstensi pdf',
                'file.max'                     => 'file sizes maks 2MB',
                'rangkuman.required'           => 'kolom rangkuman harus diisi',
                'date_open.required'           => 'kolom date_open harus diisi',
                'date_close.required'          => 'kolom date_close harus diisi',
                'mapel_id.required'            => 'kolom mapel_id harus diisi',
                'guru_id.required'             => 'kolom guru_id harus diisi',
            ]
        );
        $file       = $request->file;
        $new_file   = time().$request->mapel_id.$request->guru_id.$file->getClientOriginalName();

        $content_mapel                           = new ContentMapel;
        $content_mapel->nama_content_mapel       = $request->nama_content_mapel;
        $content_mapel->file                     = $new_file;
        $content_mapel->rangkuman                = $request->rangkuman;
        $content_mapel->open_date                = $request->date_open;
        $content_mapel->close_date               = $request->date_close;
        $content_mapel->mapel_id                 = $request->mapel_id;
        $content_mapel->guru_id                  = $request->guru_id;
        $content_mapel->save();
        $file->move('file_content_mapel', $new_file);
        return redirect('/mapel/'.$request->mapel_id);
    }
    public function edit($id)
    {        
        $guru       = Guru::all();
        $content    = ContentMapel::findOrFail($id);
        return view('content.edit', compact('guru','content'));
    }
    public function update(Request $request, $id)
    {
        // $request->validate(
        //     [
                
        //         'nama_content_mapel'    => 'required',
        //         'file'                  => 'mimes:pdf|max:2200',
        //         'rangkuman'             => 'required',
        //         'date_open'             => 'required',
        //         'date_close'            => 'required',
        //         'mapel_id'              => 'required',
        //         'guru_id'               => 'required',
        //     ],
        //     [            
        //         'nama_content_mapel.required'  => 'kolom nama_content_mapel harus diisi',
        //         'file.mimes'                   => 'file harus berekstensi pdf',
        //         'file.max'                     => 'file sizes maks 2MB',
        //         'rangkuman.required'           => 'kolom rangkuman harus diisi',
        //         'date_open.required'           => 'kolom date_open harus diisi',
        //         'date_close.required'          => 'kolom date_close harus diisi',
        //         'mapel_id.required'            => 'kolom mapel_id harus diisi',
        //         'guru_id.required'             => 'kolom guru_id harus diisi',
        //     ]
        // );
        $content_mapel    = ContentMapel::find($id);
        if ($request->has('file'))
        {
            $path       = 'file_content_mapel/';
            File::delete($content_mapel->file);
            $file       = $request->file;
            $new_file   = time(). '-'. $file->getClientOriginalName();
            $file->move('file_content_mapel/', $new_file);
            $content_mapel->file      = $new_file;
        }
        $content_mapel->nama_content_mapel       = $request->nama_content_mapel;
        $content_mapel->rangkuman                = $request->rangkuman;
        $content_mapel->open_date                = $request->date_open;
        $content_mapel->close_date               = $request->date_close;
        $content_mapel->save();

        return redirect('/mapel');
    }
    public function hapus($id)
    {
        $content = ContentMapel::find($id);
        Alert::toast("Data Content Mata Pelajaran Berhasil dihapus", 'success');
        $content->delete();
        return redirect('/mapel');
    }
}