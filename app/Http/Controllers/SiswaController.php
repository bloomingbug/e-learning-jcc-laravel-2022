<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Exports\SiswaExport;
use Maatwebsite\Excel\Facades\Excel;

use App\User;
use App\Siswa;
use App\Role;
use App\Kelas;

class SiswaController extends Controller
{   
    // Menampilkan Daftar Siswa
    public function index()
    {
        $users  = User::all();
        $siswa  = Siswa::all();
        $role   = Role::all();
        return view('siswa.index', compact('users', 'siswa', 'role'));
    }

    // Mengalihkan ke halaman create siswa
    public function create()
    {   
        $role   = Role::all();
        $kelas  = Kelas::all();
        return view('siswa.create', compact('kelas', 'role'));
    }

    // Mengirim data ke DB
    // Nantinya m_user_id tidak diinput manual tapi dari Auth
    public function store(Request $request)
    {
        $request->validate(
        [
            'nis'               => 'required',
            'nama_lengkap'      => 'required',
            'tgl_lahir'         => 'required',
            'alamat'            => 'required',
            'no_hp'             => 'required',
            'name'              => 'required|unique:m_users',
            'email'             => 'required|unique:m_users',
            'password'          => 'required|min:7',
            'role_id'           => 'required',
            'kelas_id'          => 'required'
        ],
        [
        'nis.required'          => 'Kolom NIS harus diisi yah kawan!',
        'nama_lengkap.required' => 'Kolom Nama Lengkap harus diisi yah kawan!',
        'tgl_lahir.required'    => 'Kolom Tanggal Lahir harus diisi yah kawan!',
        'alamat.required'       => 'Kolom Alamat harus diisi yah kawan!',
        'no_hp.required'        => 'Kolom Nomor Handphone harus diisi yah kawan!',
        'name.required'         => 'Kolom Username harus diisi yah kawan!',
        'name.unique'           => 'Username telah digunakan!',
        'email.required'        => 'Kolom email harus diisi yah kawan!',
        'email.unique'          => 'Email sudah terdaftar!',
        'password.required'     => 'Kolom password harus diisi yah kawan!',
        'password.min'          => 'Password minimal mengandung 8 karakter!',
        'role_id.required'      => 'Kolom Role harus diisi yah kawan!',
        'kelas_id.required'     => 'Kolom Kelas harus diisi yah kawan!',
        ]);
        $arrayUser = [
            'name'          => Str::lower($request->name),
            'email'         => $request->email,
            'password'      => Hash::make($request->password),
            'role_id'       => $request->role_id
        ];
        $user   = DB::table('m_users')->insertGetId($arrayUser);

        $arraySiswa = [
            'nis'           => $request->nis,
            'nama_lengkap'  => $request->nama_lengkap,
            'tgl_lahir'     => $request->tgl_lahir,
            'alamat'        => $request->alamat,
            'no_hp'         => $request->no_hp,
            'user_id'       => $user
        ];
        $siswa  = DB::table('m_siswa')->insertGetId($arraySiswa);

        $kelas  = [
            'siswa_id'      => $siswa,
            'kelas_id'      => $request->kelas_id
        ];
        DB::table('kelas_siswa')->insert($kelas);
        return redirect('/siswa');
    }

    // Menampilkan detail dari siswa yang dipilih
    public function show($id)
    {
        $siswa  = Siswa::findOrFail($id);
        $users  = User::all();
        $role   = Role::all();
        return view('siswa.show', compact('siswa'));
    }

    // Mengalihkan ke halaman edit data
    public function edit($id)
    {
        $siswa  = Siswa::findOrFail($id);
        $kelas  = Kelas::all();
        return view('siswa.edit', compact('siswa', 'kelas'));
    }

    // Mengirim data update ke DB
    public function update(Request $request, $id)
    {
        $request->validate(
        [
            'nama_lengkap'  => 'required',
            'tgl_lahir'     => 'required',
            'alamat'        => 'required',
            'no_hp'         => 'required',
            'kelas_id'      => 'required',
        ],
        [
        'nama_lengkap.required'     => 'Kolom Nama Lengkap harus diisi yah kawan!',
        'tgl_lahir.required'        => 'Kolom Tanggal Lahir harus diisi yah kawan!',
        'alamat.required'           => 'Kolom Alamat harus diisi yah kawan!',
        'no_hp.required'            => 'Kolom Nomor Handphone harus diisi yah kawan!',
        'kelas_id.required'         => 'Kolom Kelas harus diisi yah kawan!',
        ]);

        $arraySiswa = [
            'nama_lengkap'  => $request->nama_lengkap,
            'tgl_lahir'     => $request->tgl_lahir,
            'alamat'        => $request->alamat,
            'no_hp'         => $request->no_hp
        ];
        $siswa  = DB::table('m_siswa')->where('id', $id)->update($arraySiswa);
        
        DB::table('kelas_siswa')
                        ->where('siswa_id', $id)
                        ->update(['kelas_id' => $request->kelas_id]);

        DB::table('m_users')
                        ->where('id', $request->user_id)
                        ->update(['name' => $request->username, 'email' => $request->email]);
        
        return redirect('/siswa');
        
    }

    // Menghapus data
    public function destroy($id)
    {
        $siswa = Siswa::findOrFail($id);
        $siswa->delete();
        
        return redirect('/siswa');
    }
}