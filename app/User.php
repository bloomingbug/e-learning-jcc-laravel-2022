<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = "m_users";
    
    protected $fillable = [
        'name',
        'email',
        'role_id',
        'password',
        
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];
    
    protected $casts = [
        'email_verified_at' =>  'datetime',
    ];
    
    // Relasi dengan table role
    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id');
    }
    public function siswa()
    {
        return $this->hasOne('App\Siswa', 'siswa_id');
    }
    public function guru()
    {
        return $this->hasOne('App\Guru');
    }
}