<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\SiswaKelas;
class Siswa extends Model
{
    protected $table = "m_siswa";
    protected $fillable = [
        "nis", 
        "nama_lengkap", 
        "no_hp", 
        "tgl_lahir", 
        "alamat", 
        "user_id"];
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function siswakelas()
    {
        return $this->hasMany("SiswaKelas");
    }
    public function kelas()
    {
        return $this->belongsToMany('App\Kelas');
    }
}   