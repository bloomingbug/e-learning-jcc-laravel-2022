<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiswaKelas extends Model
{
    //
    protected $table    = 'kelas_siswa';
    protected $fillable = ['siswa_id','kelas_id'];

    public function kelas()
    {
        return $this->belongsTo('App\kelas', 'kelas_id');
    }
    public function siswa()
    {
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }
}
