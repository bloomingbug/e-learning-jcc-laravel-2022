<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mapel extends Model
{
    //
    protected $table      = "m_matapelajaran";
    protected $fillable   = [
        "kode_mapel", 
        "nama_mapel", 
        "deskripsi_mapel"
    ];
    
    public function content(){
        return $this->hasMany('App\ContentMapel');
    }
    public function guru(){
        return $this->hasMany('App\Guru');
    }

}