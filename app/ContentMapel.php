<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentMapel extends Model
{
    //
    protected $table      = "content_mapel";
    protected $fillable   = [
        "nama_content_mapel",
        "file",
        "rangkuman",
        "open_date",
        "close_date",
        "mapel_id",
        "guru_id"
    ];
    public function mapel()
    {
        return $this->belongsTo('App\Mapel', 'mapel_id');
    }
    public function guru()
    {
        return $this->belongsTo('App\Guru', 'guru_id');
    }
}