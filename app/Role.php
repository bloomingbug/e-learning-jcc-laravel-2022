<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = "m_role";
    protected $fillable = [
        "id", 
        "nama"
    ];

    // Relation ke Table User
    public function user(){
        return $this    ->  hasMany('App\User');
    }
}