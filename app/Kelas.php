<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    // Model kelas menghubungka ke tabel m_kelas
    protected $table    = 'm_kelas';
    protected $fillable = [
        'kode_kelas',
        'nama_kelas'
    ];
    public function siswakelas()
    {
        return $this->hasMany("App\SiswaKelas");
    }
    public function siswa(){
        return $this->belongsToMany('App\Siswa');
    }
}
