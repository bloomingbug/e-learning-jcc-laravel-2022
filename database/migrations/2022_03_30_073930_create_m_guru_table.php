<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMGuruTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_guru', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('nik');
            $table->string('nama');
            $table->date('tgl_lahir');
            $table->text('alamat');
            $table->string('no_hp');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('m_users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_guru');
    }
}