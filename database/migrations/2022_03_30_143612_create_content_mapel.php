<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentMapel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_mapel', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_content_mapel', 100);
            $table->string('file', 100);
            $table->longText('rangkuman');
            $table->date('open_date');
            $table->date('close_date');
            $table->unsignedBigInteger('mapel_id');
            $table->foreign('mapel_id')->references('id')->on('m_matapelajaran');
            $table->unsignedBigInteger('guru_id');
            $table->foreign('guru_id')->references('id')->on('m_guru');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_mapel');
    }
}