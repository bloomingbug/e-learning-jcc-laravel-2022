@extends('adminlte.master')

@section('title')
    | Mata Pelajaran
@endsection

@section('judul')
    Mata Pelajaran
@endsection


@section('link')
    @for ($i = 1; $i <= count(Request::segments()); $i++)
        @if ($i == count(Request::segments()))
            <li class="breadcrumb-item active">
                {{ ucfirst(trans(Request::segment($i))) }}
            </li>
        @else
            <li class="breadcrumb-item">
                <a href="{{ url('/' . Request::segment($i)) }}">{{ ucfirst(trans(Request::segment($i))) }}</a>
            </li>
        @endif
    @endfor
@endsection

@section('content')
    <section class="content">
        <!-- /.card -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Daftar Mata Pelajaran</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        @forelse ($mapel as $key => $item)
                            <!-- we are adding the accordion ID so Bootstrap's collapse plugin detects it -->
                            <div id="accordion">
                                <div class="card card-primary">
                                    <div class="card-header">
                                        <h4 class="card-title w-100">
                                            <a class="d-block w-100 collapsed" data-toggle="collapse"
                                                href="#collaps-{{ $key }}" aria-expanded="false">
                                                {{ $item->kode_mapel . ' | ' . $item->nama_mapel }}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collaps-{{ $key }}" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <p class="card-text">
                                                {{ $item->deskripsi_mapel }}
                                            </p>
                                        </div>
                                        <div class="card-footer">
                                            <a href="/mapelsiswa/{{ $item->id }}" class="btn btn-success btn-sm"
                                                data-toggle="tooltip" data-placement="top"
                                                title="Masuk Mapel {{ $item->nama_mapel }}">
                                                <i class="fa fa-info-circle" aria-hidden="true"></i> Akses
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="card">
                                <div class="card-body">
                                    <h1>Tidak ada mata pelajaran</h1>
                                </div>
                            </div>
                        @endforelse
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>

    </section>
    <!-- /.content -->
@endsection

@push('scripts')
    <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#mapel").DataTable();
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.btn-hapus').click(function(event) {
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: `Yakin Bakal Hapus Data ini?`,
                    text: "JIka Anda Yakin Data Pasti Bakal Hilang..!!!",
                    icon: "warning",
                    buttons: ["Tidak", "Yakin Lah.."],
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
@endpush
