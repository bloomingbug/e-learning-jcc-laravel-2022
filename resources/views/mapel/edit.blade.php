@extends('adminlte.master')

@section('title')
    | Edit Mata Pelajaran
@endsection

@section('judul')
    Halaman Edit Mata Pelajaran
@endsection

@section('link')
    @for ($i = 1; $i <= count(Request::segments()); $i++)
        @if ($i == count(Request::segments()))
            <li class="breadcrumb-item active">
                {{ ucfirst(trans(Request::segment($i))) }}
            </li>
        @else
            <li class="breadcrumb-item">
                <a href="{{ url('/' . Request::segment($i)) }}">{{ ucfirst(trans(Request::segment($i))) }}</a>
            </li>
        @endif
    @endfor
@endsection

@section('content')
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Mata Pelajaran</h3>
            </div>
            <div class="card-body">
                <!-- form start -->
                <form action="/mapel/{{ $mapel->id }}" method="POST">
                    @csrf
                    @method('put')
                    <div class="card-body">
                        <div class="form-group">
                            <label for="kode_mapel">Kode Mata Pelajaran</label>
                            <input type="text" class="form-control" name="kode_mapel" id="kode_mapel"
                                value="{{ $mapel->kode_mapel }}">
                        </div>
                        @error('kode_mapel')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label for="nama_mapel">Nama Mata Pelajaran</label>
                            <input type="text" class="form-control" name="nama_mapel" id="nama_mapel"
                                value="{{ $mapel->nama_mapel }}">
                        </div>
                        @error('nama_mapel')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label for="deskripsi_mapel">Deskripsi Mata Pelajaran</label>
                            <textarea name="deskripsi_mapel" class="form-control" id="deskripsi_mapel" cols="30"
                                rows="10">{{ $mapel->deskripsi_mapel }}</textarea>
                        </div>
                        @error('deskripsi_mapel')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="card-footer">
                        <a href="/mapel" class="btn btn-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                            Kembali</a>
                        <button type="submit" class="btn btn-primary show_confirm"><i class="fa fa-save"
                                aria-hidden="true"></i> Simpan Perubahan</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.show_confirm').click(function(event) {
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: `Yakin Bakal Edit Data ini?`,
                    text: "JIka Anda Yakin Data Akan Disimpan..!",
                    icon: "warning",
                    buttons: ["Tidak", "Yakin Lah.."],
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
@endpush
