@extends('adminlte.master')

@section('title')
    | Mata Pelajaran
@endsection

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css" />
@endpush

@section('link')
    @for ($i = 1; $i <= count(Request::segments()); $i++)
        @if ($i == count(Request::segments()))
            <li class="breadcrumb-item active">
                {{ ucfirst(trans(Request::segment($i))) }}
            </li>
        @else
            <li class="breadcrumb-item">
                <a href="{{ url('/' . Request::segment($i)) }}">{{ ucfirst(trans(Request::segment($i))) }}</a>
            </li>
        @endif
    @endfor
@endsection

@section('judul')
    Daftar Mata Pelajaran
@endsection

@section('content')
    <section class="content">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <a href="/mapel/create" class="btn btn-primary">Tambah Mata Pelajaran</a>
                <a href="/export/mapel" class="btn btn-info ml-auto">Export</a>
            </div>
            <div class="card-body">
                @can('admin', Auth::user())
                    <div class="row text-right">
                        <div class="col-lg-6">
                        </div>
                        <div class="col-lg-6">
                        </div>
                    </div>
                @endcan

                <table id="mapel" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Mata Pelajaran</th>
                            <th>Nama Mata Pelajaran</th>
                            <th style="text-align:center; vertical-align: inherit; width: 150px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($mapel as $key => $item)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $item->kode_mapel }}</td>
                                <td>{{ $item->nama_mapel }}</td>
                                <td class="text-center">
                                    <form action="/mapel/{{ $item->id }}" method="POST">
                                        @csrf
                                        <a href="/mapel/{{ $item->id }}" class="btn btn-info btn-sm"
                                            data-toggle="tooltip" data-placement="top" title="Detail"><i
                                                class="fa fa-info-circle" aria-hidden="true"></i> </a>
                                        @method('delete')
                                        @can('admin', Auth::user())
                                            <a href="/mapel/{{ $item->id }}/edit " class="btn btn-warning btn-sm"
                                                data-toggle="tooltip" data-placement="top" title="Edit"><i
                                                    class="fa fa-edit" aria-hidden="true"></i> </a>
                                            <button type="submit" class="btn btn-danger btn-sm btn-hapus" data-toggle="tooltip"
                                                data-placement="top" title="Delete">
                                                <i class="fa fa-trash fa-sm"></i>
                                            </button>
                                        @endcan
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <h3>Data Kosong</h3>
                        @endforelse
                </table>
            </div>
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection

@push('scripts')
    <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#mapel").DataTable();
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.btn-hapus').click(function(event) {
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: `Yakin Bakal Hapus Data ini?`,
                    text: "JIka Anda Yakin Data Pasti Bakal Hilang..!!!",
                    icon: "warning",
                    buttons: ["Tidak", "Yakin Lah.."],
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
@endpush
