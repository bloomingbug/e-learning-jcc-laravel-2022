@extends('adminlte.master')

@section('title')
    | Tambah Mata Pelajaran
@endsection

@section('judul')
    Tambah Data Mata Pelajaran
@endsection

@section('link')
    @for ($i = 1; $i <= count(Request::segments()); $i++)
        @if ($i == count(Request::segments()))
            <li class="breadcrumb-item active">
                {{ ucfirst(trans(Request::segment($i))) }}
            </li>
        @else
            <li class="breadcrumb-item">
                <a href="{{ url('/' . Request::segment($i)) }}">{{ ucfirst(trans(Request::segment($i))) }}</a>
            </li>
        @endif
    @endfor
@endsection

@section('content')
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Mata Pelajaran</h3>
            </div>
            <div class="card-body">
                <!-- form start -->
                <form action="/mapel" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="kode_mapel">Kode Mata Pelajaran</label>
                        <input type="text" class="form-control" name="kode_mapel" id="kode_mapel"
                            placeholder="Enter Kode Mata Pelajaran Ex. MTK011">
                    </div>
                    @error('kode_mapel')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <label for="nama_mapel">Nama Mata Pelajaran</label>
                        <input type="text" class="form-control" name="nama_mapel" id="nama_mapel"
                            placeholder="Enter Nama Mata Pelajaran Ex. Matematika">
                    </div>
                    @error('nama_mapel')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <label for="deskripsi_mapel">Deskripsi Mata Pelajaran</label>
                        <textarea name="deskripsi_mapel" class="form-control" id="deskripsi_mapel" cols="30" rows="10"
                            placeholder="Enter Deskripsi Mata Pelajaran Ex. Matematika (dari bahasa Yunani: μαθημα - mathēma, 'pengetahuan, pemikiran, pembelajaran' atau sebelumnya disebut ilmu hisab adalah ilmu yang mempelajari besaran, struktur, ruang, dan perubahan. Para matematikawan merangkai dan menggunakan berbagai pola, kemudian menggunakannya untuk merumuskan konjektur baru, dan membangun kebenaran melalui metode deduksi yang ketat diturunkan dari aksioma-aksioma dan definisi-definisi yang bersesuaian."></textarea>
                    </div>
                    @error('deskripsi_mapel')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <a href="/mapel" class="btn btn-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i>
                        Simpan</button>
                </form>
            </div>
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection
