@extends('adminlte.master')

@section('title')
    | Detail Mapel
@endsection

@section('judul')
    Detail Mata Pelajaran
@endsection

@section('link')
    @for ($i = 1; $i <= count(Request::segments()); $i++)
        @if ($i == count(Request::segments()))
            <li class="breadcrumb-item active">
                {{ ucfirst(trans(Request::segment($i))) }}
            </li>
        @else
            <li class="breadcrumb-item">
                <a href="{{ url('/' . Request::segment($i)) }}">{{ ucfirst(trans(Request::segment($i))) }}</a>
            </li>
        @endif
    @endfor
@endsection

@section('content')
    <section class="content">
        <div class="card bg-dark">
            <div class="card-header">{{ $mapel->kode_mapel }} | {{ $mapel->nama_mapel }}</div>
            <div class="card-body">
                <p class="card-text">{{ $mapel->deskripsi_mapel }}</p>
            </div>
        </div>
        @can('guru', Auth::user())
            <div class="row text-right">
                <div class="col-lg-6">
                </div>
                <div class="col-lg-6">
                    <a href="/mapel/{{ $mapel->id }}/content/create" class="btn btn-primary my-3 btn-md">
                        Tambah Materi Pelajaran
                    </a>
                </div>
            </div>
        @endcan

        @forelse ($mapel->content as $key => $item)
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-book mr-1"></i>
                        {{ $item->nama_content_mapel }}
                    </h3>
                    <div class="card-tools">
                        <ul class="nav nav-pills ml-auto">
                            <li class="nav-item">
                                <a class="nav-link active" href="#rangkuman{{ $item->id }}"
                                    data-toggle="tab">Rangkuman</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#materi{{ $item->id }}" data-toggle="tab">Materi</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content p-0">
                        <div class="chart tab-pane active" id="rangkuman{{ $item->id }}"
                            style="position: relative; height: 200px;">
                            <p class="card-text">{{ $item->rangkuman }}</p>
                        </div>
                        <div class="chart tab-pane" id="materi{{ $item->id }}"
                            style="position: relative; height: 600px;">
                            <embed src="{{ url('/' . 'file_content_mapel/' . $item->file) }}" frameborder="0"
                                width="100%" height="600px">
                        </div>
                    </div>
                </div>
                @can('guru', Auth::user())
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-8">
                                <form action="/mapel/content/{{ $item->id }}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <a href="/mapel/content/edit/{{ $item->id }}" class="btn btn-warning btn-sm"><i
                                            class="fa fa-edit" aria-hidden="true"></i> Edit</a>
                                    <button type="submit" class="btn btn-danger btn-sm btn-hapus">
                                        <i class="fa fa-trash fa-sm"></i> Hapus Materi
                                    </button>
                                </form>
                            </div>
                            <div class="col-4 text-right">
                                <div class="custom-control custom-checkbox">
                                    <p class="card-text">Author : {{ Str::upper($item->guru->nama) }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endcan
            </div>

        @empty
            <div class="card text-white bg-warning">
                <div class="card-body">
                    <p class="card-text">Data Materi Pelajaran Masih Kosong..!</p>
                </div>
            </div>
        @endforelse

    </section>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.btn-hapus').click(function(event) {
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: `Yakin Bakal Hapus Data ini?`,
                    text: "JIka Anda Yakin Data Pasti Bakal Hilang..!!!",
                    icon: "warning",
                    buttons: ["Tidak", "Yakin Lah.."],
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
@endpush
