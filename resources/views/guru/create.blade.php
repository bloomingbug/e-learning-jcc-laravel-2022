@extends('adminlte.master')

@section('title')
    | Tambah Guru
@endsection

@section('judulutama')
    Tambah Guru
@endsection

@section('judul')
    Form Tambah Guru
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="/guru" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="card border border-secondary card-secondary">
                            <div class="card-header">
                                <h5 class="card-text">Pribadi</h5>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="nik">NIK</label>
                                    <input id="nik" type="number" class="form-control" name="nik" min="0"
                                        value="{{ old('nik') }}">
                                </div>
                                @error('nik')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group">
                                    <label for="nama">Nama Lengkap</label>
                                    <input id="nik" type="text" class="form-control" name="nama"
                                        value="{{ old('nama') }}">
                                </div>
                                @error('nama')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group">
                                    <label for="tgl_lahir">Tanggal Lahir</label>
                                    <input id="tgl_lahir" type="date" class="form-control" name="tgl_lahir"
                                        value="{{ old('tgl_lahir') }}">
                                </div>
                                @error('tgl_lahir')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group">
                                    <label for="no_hp">Nomor Handphone</label>
                                    <input id="no_hp" type="text" class="form-control" name="no_hp"
                                        value="{{ old('no_hp') }}">
                                </div>
                                @error('no_hp')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <textarea id="alamat" name="alamat" class="form-control" rows="5.9">{{ old('alamat') }}</textarea>
                                </div>
                                @error('alamat')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card border border-secondary card-secondary">
                            <div class="card-header">
                                <h5 class="card-text">User</h5>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Username</label>
                                    <input type="text" class="form-control" id="name" name="name"
                                        value="{{ old('name') }}">
                                </div>
                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group">
                                    <label for="email">E-Mail</label>
                                    <input type="text" class="form-control" id="email" name="email"
                                        value="{{ old('email') }}">
                                </div>
                                @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="password" name="password">
                                </div>
                                @error('password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group">
                                    <label for="role_id">Role</label>
                                    <select name="role_id" id="role_id" class="form-control">
                                        <option value="">---Pilih Role---</option>
                                        @foreach ($role as $item)
                                            <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @error('role_id')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <a href="/guru" class="btn btn-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i>
                    Simpan</button>
            </form>
        </div>
    </div>
@endsection
