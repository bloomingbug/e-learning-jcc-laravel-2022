@extends('adminlte.master')

@section('title')
    | Edit Guru
@endsection

@section('judul')
    Edit of {{ $guru->nama }}
@endsection

@section('link')
    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection



@section('content')
    <div class="card">

        <div class="card-body">
            <form action="/guru/{{ $guru->id }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>NIK</label>
                    <input type="number" class="form-control" name="nik" min="0" value="{{ $guru->nik }}">
                </div>
                @error('nik')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>User Name</label>
                    <input type="text" class="form-control" name="name" value="{{ $guru->user->name }}">
                </div>
                @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>e-mail</label>
                    <input type="text" class="form-control" name="email" value="{{ $guru->user->email }}">
                </div>
                @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama" value="{{ $guru->nama }}">
                </div>
                @error('nama_lengkap')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <input type="date" class="form-control" name="tgl_lahir" value="{{ $guru->tgl_lahir }}">
                </div>
                @error('tgl_lahir')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Nomor Handphone</label>
                    <input type="text" class="form-control" name="no_hp" value="{{ $guru->no_hp }}">
                </div>
                @error('no_hp')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Alamat</label>
                    <textarea name="alamat" class="form-control">{{ $guru->alamat }}</textarea>
                </div>
                @error('alamat')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <a href="/guru" class="btn btn-secondary btn-md">Kembali</a>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
