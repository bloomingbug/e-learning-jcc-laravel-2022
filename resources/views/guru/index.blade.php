@extends('adminlte.master')

@section('title')
    | Guru
@endsection

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css" />
@endpush

@section('link')
    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
    <li class="breadcrumb-item active">Guru</li>
@endsection


@section('judul')
    Daftar Guru
@endsection

@section('content')
    <div class="card">
        <div class="card-header d-flex justify-between">
            <a href="/guru/create" class="btn btn-primary btn-md">Tambah Guru</a>
            <a href="/export/guru" class="btn btn-info ml-auto btn-md">Export</a>
        </div>
        <div class="card-body">
            <table id="data-tables" class="table table-bordered table-hover mt-3">
                <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">NIK</th>
                        <th scope="col">Nama Lengkap</th>
                        <th scope="col">Tanggal Lahir</th>
                        <th scope="col">Email</th>
                        <th scope="col">Nomor HP</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Role</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($guru as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $item->nik }}</td>
                            <td>{{ $item->nama }}</td>
                            <td>{{ $item->tgl_lahir }}</td>
                            <td>{{ $item->user->email }}</td>
                            <td>{{ $item->no_hp }}</td>
                            <td>{{ Str::limit($item->alamat, 20) }}</td>
                            <td>{{ $item->user->role->nama }}</td>
                            <td>
                                <div class="d-flex flex-nowrap flex-column flex-md-row justify-center">
                                    <a href="/guru/{{ $item->id }}" class="btn btn-info btn-sm m-1">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                    </a>
                                    <a href="/guru/{{ $item->id }}/edit" class="btn btn-warning btn-sm m-1">
                                        <i class="fa fa-edit" aria-hidden="true">
                                        </i>
                                    </a>
                                    <form action="/guru/{{ $item->id }}" method="post" class="m-1 p-0">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm mx-auto w-100">
                                            <i class="fa fa-trash fa-sm"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>

                    @empty
                        <p>Data Guru Kosong!</p>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#data-tables").DataTable();
        });
    </script>
@endpush
