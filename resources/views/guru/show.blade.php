@extends('adminlte.master')

@section('title')
    | Detail Guru
@endsection

@section('judul')
    {{ $guru->nama }}
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th scope="col">NIK</th>
                        <td scope="col">{{ $guru->nik }}</td>
                    </tr>
                    <tr>
                        <th scope="col">Nama Lengkap</th>
                        <td scope="col">{{ $guru->nama }}</td>
                    </tr>
                    <tr>
                        <th scope="col">Tanggal Lahir</th>
                        <td scope="col">{{ $guru->tgl_lahir }}</td>
                    </tr>
                    <tr>
                        <th scope="col">E-Mail</th>
                        <td scope="col">{{ $guru->user->email }}</td>
                    </tr>
                    <tr>
                        <th scope="col">Nomor Handphone</th>
                        <td scope="col">{{ $guru->no_hp }}</td>
                    </tr>
                    <tr>
                        <th scope="col">Alamat</th>
                        <td scope="col">{{ $guru->alamat }}</td>
                    </tr>
                    <tr>
                        <th scope="col">Role</th>
                        <td scope="col">{{ $guru->user->role->nama }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            <a href="/guru" class="btn btn-secondary btn-md ml-auto">Kembali</a>
        </div>
    </div>
@endsection
