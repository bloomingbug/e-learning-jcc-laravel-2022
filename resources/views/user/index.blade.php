@extends('adminlte.master')

@section('title')
    | User
@endsection

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css" />
@endpush

@section('link')
    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
    <li class="breadcrumb-item active">User</li>
@endsection

@section('title')
    | Daftar User
@endsection

@section('judul')
    Daftar User
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <a href="/user/create" class="btn btn-primary btn-md">Tambah User</a>
        </div>
        <div class="card-body">
            <table id="data-tables" class="table table-bordered table-hover mt-3">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($users as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->role->nama }}</td>
                            <td>
                                <div class="d-flex flex-wrap flex-column flex-md-row justify-center">
                                    <a href="/user/{{ $item->id }}" class="btn btn-info btn-sm m-1">
                                        <i class="fa fa-info-circle" aria-hidden="true">
                                        </i>
                                    </a>
                                    <a href="/user/{{ $item->id }}/edit" class="btn btn-warning btn-sm m-1">
                                        <i class="fa fa-edit" aria-hidden="true">
                                        </i>
                                    </a>
                                    <form action="/user/{{ $item->id }}" method="post" class="m-1 p-0">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm mx-auto w-100">
                                            <i class="fa fa-trash fa-sm"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <p>Data User Kosong!</p>
                    @endforelse
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#data-tables").DataTable();
        });
    </script>
@endpush
