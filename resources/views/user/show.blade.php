@extends('adminlte.master')

@section('title')
    | Detail User
@endsection

@section('judulutama')
    Detail of {{ $user->nama }}
@endsection

@section('judul')
    {{ $user->nama }}
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th scope="col">ID</th>
                        <td scope="col">: {{ $user->id }}</td>
                    </tr>
                    <tr>
                        <th scope="col">Nama Lengkap</th>
                        <td scope="col">: {{ $user->name }}</td>
                    </tr>
                    <tr>
                        <th scope="col">Email</th>
                        <td scope="col">: {{ $user->email }}</td>
                    </tr>
                    <tr>
                        <th scope="col">Role</th>
                        <td scope="col">: {{ $user->role->nama }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            <a href="/user" class="btn btn-secondary btn-md">Kembali</a>
        </div>
    </div>
@endsection
