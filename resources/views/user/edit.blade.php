@extends('adminlte.master')

@section('title')
    | Edit User
@endsection

@section('judulutama')
    Edit User
@endsection

@section('judul')
    Edit {{ $user->name }}
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="/user/{{ $user->id }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Nama Lengkap</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}">
                </div>
                @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="email">E-Mail</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{ $user->email }}">
                </div>
                @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" value="xxxxxxxxxx" disabled>
                </div>
                @error('password')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="role_id">Role</label>
                    <select name="role_id" id="role_id" class="form-control">
                        <option value="">---Pilih Genre---</option>
                        @foreach ($role as $item)
                            <option value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endforeach
                    </select>
                </div>
                @error('role_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <a href="/user" class="btn btn-secondary btn-md">Kembali</a>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
