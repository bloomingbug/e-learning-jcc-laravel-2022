@extends('adminlte.master')

@section('title')
    | Tambah User
@endsection

@section('title')
    | Form Tambah User
@endsection

@section('link')
    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection

@section('judul')
    Form Tambah User
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="/user" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Username</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="email">E-Mail</label>
                    <input type="text" class="form-control" id="email" name="email">
                </div>
                @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                @error('password')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="role_id">Role</label>
                    <select name="role_id" id="role_id" class="form-control">
                        <option value="">---Pilih Role---</option>
                        @foreach ($role as $item)
                            <option value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endforeach
                    </select>
                </div>
                @error('role_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <a href="/siswa" class="btn btn-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i>
                    Simpan</button>
            </form>
        </div>
    </div>
@endsection
