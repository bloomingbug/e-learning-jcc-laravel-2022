@extends('adminlte.master')

@section('title')
    | Detail Role
@endsection

@section('judulutama')
    Role {{ $role->nama }}
@endsection

@section('judul')
    Daftar User Role {{ $role->nama }}
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <table class="table table-bordered table-hover mt-3">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Email</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($role->user as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->email }}</td>
                        </tr>

                    @empty
                        <p>Data Masih Kosong!</p>
                    @endforelse
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            <a href="/role" class="btn btn-secondary btn-md">Kembali</a>
        </div>
    </div>
@endsection
