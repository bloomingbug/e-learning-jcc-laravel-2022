@extends('adminlte.master')

@section('title')
    | Edit Role
@endsection

@section('judulutama')
    Edit Role
@endsection

@section('judul')
    Form Edit Role
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="/role/{{ $role->id }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="nama">Nama Role</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{ $role->nama }}">
                </div>
                @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <a href="/role" class="btn btn-secondary btn-md">Kembali</a>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
