@extends('adminlte.master')

@section('title')
    | Role
@endsection

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css" />
@endpush

@section('link')
    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
    <li class="breadcrumb-item active">Role</li>
@endsection


@section('judul')
    Daftar Role
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <a href="/role/create" class="btn btn-primary btn-md">Tambah Role</a>
        </div>
        <div class="card-body">
            <table id="data-tables" class="table table-bordered table-hover mt-3">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Role</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($role as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $item->nama }}</td>
                            <td>
                                <div class="d-flex flex-wrap flex-column flex-md-row justify-center">
                                    <a href="/role/{{ $item->id }}" class="btn btn-info btn-sm m-1">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                    </a>
                                    <a href="/role/{{ $item->id }}/edit" class="btn btn-warning btn-sm m-1">
                                        <i class="fa fa-edit" aria-hidden="true">
                                        </i>
                                    </a>
                                    <form action="/role/{{ $item->id }}" method="post" class="m-1 p-0">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-hapus btn-danger btn-sm mx-auto w-100">
                                            <i class="fa fa-trash fa-sm"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>

                    @empty
                        <p>Data Role Kosong!</p>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#data-tables").DataTable();
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.btn-hapus').click(function(event) {
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: `Yakin Bakal Hapus Data ini?`,
                    text: "JIka Anda Yakin Data Pasti Bakal Hilang..!!!",
                    icon: "warning",
                    buttons: ["Tidak", "Yakin Lah.."],
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
@endpush
