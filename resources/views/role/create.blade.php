@extends('adminlte.master')

@section('title')
    | Tambah Role
@endsection

@section('judulutama')
    Tambah Role
@endsection

@section('judul')
    Form Tambah Role
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="/role" method="post">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama Role</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Contoh: Guru">
                </div>
                @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <a href="/siswa" class="btn btn-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i>
                    Simpan</button>
            </form>
        </div>
    </div>
@endsection
