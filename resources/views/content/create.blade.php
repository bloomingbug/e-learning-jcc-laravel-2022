@extends('adminlte.master')

@section('judul')
    Tambah Data Content Mata Pelajaran
@endsection

@section('link')
    @for ($i = 1; $i <= count(Request::segments()); $i++)
        @if ($i == count(Request::segments()))
            <li class="breadcrumb-item active">
                {{ ucfirst(trans(Request::segment($i))) }}
            </li>
        @else
            <li class="breadcrumb-item">
                <a href="{{ url('/' . Request::segment($i)) }}">{{ ucfirst(trans(Request::segment($i))) }}</a>
            </li>
        @endif
    @endfor
@endsection

@section('content')
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Content Mata Pelajaran</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <!-- form start -->
                <form action="/mapel/content" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="text" name="mapel_id" id="mapel_id" hidden value="{{ $mapel_id }}">
                    <input type="text" name="guru_id" id="guru_id" hidden value="{{ Auth::user()->guru->id }}">
                    <div class="form-group">
                        <label for="nama_content_mapel">Nama Materi</label>
                        <input type="text" class="form-control" name="nama_content_mapel" id="nama_content_mapel"
                            placeholder="Enter Nama Materi Pelajaran">
                    </div>
                    @error('nama_content_mapel')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <label for="file">Materi Pelajaran / Bahan Ajar :</label>
                        <input type="file" name="file" class="form-control" id="file" placeholder="Chose File">
                    </div>
                    @error('file')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <label for="rangkuman">Rangkuman Materi :</label>
                        <textarea name="rangkuman" class="form-control" id="rangkuman" cols="30" rows="10"
                            placeholder="Enter Deskripsi Mata Pelajaran Ex. Matematika (dari bahasa Yunani: μαθημα - mathēma, 'pengetahuan, pemikiran, pembelajaran' atau sebelumnya disebut ilmu hisab adalah ilmu yang mempelajari besaran, struktur, ruang, dan perubahan. Para matematikawan merangkai dan menggunakan berbagai pola, kemudian menggunakannya untuk merumuskan konjektur baru, dan membangun kebenaran melalui metode deduksi yang ketat diturunkan dari aksioma-aksioma dan definisi-definisi yang bersesuaian."></textarea>
                    </div>
                    @error('rangkuman')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <!-- Date and time open -->
                    <div class="form-group">
                        <label>Tanggal Dibuka : </label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" id="datemask_open" name="date_open" class="form-control"
                                data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy-mm-dd" data-mask>
                        </div>
                    </div>
                    <!-- Date and time close -->
                    <div class="form-group">
                        <label>Tanggal Ditutup : </label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" id="datemask_close" name="date_close" class="form-control"
                                data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy-mm-dd" data-mask>
                        </div>
                    </div>
                    <a href="/mapel" class="btn btn-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i>
                        Simpan</button>
                </form>
            </div>
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection

@push('scripts')
    <script src="{{ asset('adminlte/plugins/inputmask/jquery.inputmask.min.js') }}"></script>
    <script>
        $(function() {
            //Datemask yyyy-mm-dd
            $('#datemask_open').inputmask('yyyy-mm-dd', {
                'placeholder': 'yyyy-mm-dd'
            })
            $('#datemask_close').inputmask('yyyy-mm-dd', {
                'placeholder': 'yyyy-mm-dd'
            })
        });
    </script>
@endpush
