@extends('adminlte.master')

@section('judul')
    Edit Data Content Mata Pelajaran
@endsection

@section('link')
    @for ($i = 1; $i <= count(Request::segments()); $i++)
        @if ($i == count(Request::segments()))
            <li class="breadcrumb-item active">
                {{ ucfirst(trans(Request::segment($i))) }}
            </li>
        @else
            <li class="breadcrumb-item">
                <a href="{{ url('/' . Request::segment($i)) }}">{{ ucfirst(trans(Request::segment($i))) }}</a>
            </li>
        @endif
    @endfor
@endsection

@section('content')
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Content Mata Pelajaran</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <!-- form start -->
                <form action="/content/{{ $content->id }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <input type="text" name="mapel_id" id="mapel_id" hidden value="{{ $content->id }}">
                    <div class="form-group">
                        <label for="guru">Guru :</label>
                        <select name="guru_id" id="" class="form-control">
                            <option value="" disabled>--Pilih Guru--</option>
                            @foreach ($guru as $item)
                                <option value="{{ $item->id }}">{{ Str::upper($item->nama) }}</option>
                            @endforeach
                        </select>
                    </div>
                    @error('guru_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <label for="nama_content_mapel">Nama Materi</label>
                        <input type="text" class="form-control" name="nama_content_mapel" id="nama_content_mapel"
                            value="{{ $content->nama_content_mapel }}">
                    </div>
                    @error('nama_content_mapel')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <label for="file">Materi Pelajaran / Bahan Ajar :</label>
                        <input type="file" name="file" class="form-control" id="file" placeholder="Chose File">
                    </div>
                    @error('file')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <label for="rangkuman">Rangkuman Materi :</label>
                        <textarea name="rangkuman" class="form-control" id="rangkuman" cols="30"
                            rows="10">{{ $content->rangkuman }}</textarea>
                    </div>
                    @error('rangkuman')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <!-- Date and time open -->
                    <div class="form-group">
                        <label>Tanggal Dibuka : </label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" id="datemask_open" name="date_open" class="form-control"
                                data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy-mm-dd" data-mask
                                value="{{ $content->open_date }}">
                        </div>
                    </div>
                    <!-- Date and time close -->
                    <div class="form-group">
                        <label>Tanggal Ditutup : </label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" id="datemask_close" name="date_close" class="form-control"
                                data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy-mm-dd" data-mask
                                value="{{ $content->close_date }}">
                        </div>
                    </div>
                    <a href="/mapel" class="btn btn-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                    <button type="submit" class="btn btn-primary show_confirm"><i class="fa fa-pen"
                            aria-hidden="true"></i> Update</button>
                </form>
            </div>
        </div>
        <!-- /.card -->

    </section>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.show_confirm').click(function(event) {
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: `Yakin Bakal Edit Data ini?`,
                    text: "JIka Anda Yakin Data Akan Disimpan..!",
                    icon: "warning",
                    buttons: ["Tidak", "Yakin Lah.."],
                    dangerMode: false,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
    <script src="{{ asset('adminlte/plugins/inputmask/jquery.inputmask.min.js') }}"></script>
    <script>
        $(function() {
            //Datemask yyyy-mm-dd
            $('#datemask_open').inputmask('yyyy-mm-dd', {
                'placeholder': 'yyyy-mm-dd'
            })
            $('#datemask_close').inputmask('yyyy-mm-dd', {
                'placeholder': 'yyyy-mm-dd'
            })
        });
    </script>
@endpush
