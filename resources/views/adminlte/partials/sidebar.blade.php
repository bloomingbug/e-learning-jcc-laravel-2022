<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
        <img src="{{ asset('adminlte/dist/img/LogoJCC-mobile.svg') }}" alt="AdminLTE Logo" class="brand-image"
            style="opacity: .8">
        <span class="brand-text font-weight-light">E-Learning</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                    alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ Auth::user()->name }} ({{ Auth::user()->role->nama }})</a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="/" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                    {{-- Hanya Bisa Diakses Siswa --}}
                    @can('siswa', Auth::user())
                    <li class="nav-item">
                        <a href="/profile-siswa" class="nav-link">
                            <i class="fa fa-user-graduate nav-icon text-info"></i>
                            <p>Profile Siswa</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/mapelsiswa" class="nav-link">
                            <i class="fa fa-shapes nav-icon text-info"></i>
                            <p>Mata Pelajaran</p>
                        </a>
                    </li>
                @endcan
                {{-- Hanya bisa diakses Guru --}}
                @can('guru', Auth::user())
                    <li class="nav-item">
                        <a href="/profile-guru" class="nav-link">
                            <i class="fa fa-chalkboard-teacher nav-icon text-info"></i>
                            <p>Profile Guru</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/mapel" class="nav-link">
                            <i class="fa fa-shapes nav-icon text-info"></i>
                            <p>Mata Pelajaran</p>
                        </a>
                    </li>
                @endcan
                {{-- Hanya Bisa Diakses Admin --}}
                @can('admin', Auth::user())
                    <li class="nav-header">MASTER DATA</li>
                    <li class="nav-item">
                        <a href="/role" class="nav-link">
                            <i class="fa fa-user-tag nav-icon text-info"></i>
                            <p>Role</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/user" class="nav-link">
                            <i class="fa fa-user-circle nav-icon text-info"></i>
                            <p>Users</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/guru" class="nav-link">
                            <i class="fa fa-chalkboard-teacher nav-icon text-info"></i>
                            <p>Guru</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/siswa" class="nav-link">
                            <i class="fa fa-user-graduate nav-icon text-info"></i>
                            <p>Siswa</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/kelas" class="nav-link">
                            <i class="fa fa-school nav-icon text-info"></i>
                            <p>Kelas</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/mapel" class="nav-link">
                            <i class="fa fa-shapes nav-icon text-info"></i>
                            <p>Mata Pelajaran</p>
                        </a>
                    </li>
                @endcan
                <li class="nav-header">
                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-danger w-100">Logout</button>
                    </form>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
