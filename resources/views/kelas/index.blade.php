@extends('adminlte.master')

@section('title')
    | Kelas
@endsection

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css" />
@endpush

@section('judul')
    Daftar Kelas
@endsection

@section('content')
    <section class="content">
        <div class="card">
            <div class="card-header d-flex justify-content-between ">
                <a href="/kelas/create" class="btn btn-primary">Tambah Kelas</a>
                <a href="/export/kelas" class="btn btn-info ml-auto">Export</a>
            </div>
            <div class="card-body">
                <table id="kelas" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Kelas</th>
                            <th>Nama Kelas</th>
                            <th style="text-align:center; vertical-align: inherit; width: 150px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($kelas as $key => $item)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $item->kode_kelas }}</td>
                                <td>{{ $item->nama_kelas }}</td>
                                <td>
                                    <form action="/kelas/{{ $item->id }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <a href="/kelas/{{ $item->id }}" class="btn btn-info btn-sm"
                                            data-toggle="tooltip" data-placement="top" title="Detail"><i
                                                class="fa fa-info-circle" aria-hidden="true"></i> </a>
                                        <a href="/kelas/{{ $item->id }}/edit " class="btn btn-warning btn-sm"
                                            data-toggle="tooltip" data-placement="top" title="Edit"><i
                                                class="fa fa-edit" aria-hidden="true"></i> </a>
                                        <button type="submit" class="btn btn-danger btn-sm btn-hapus" data-toggle="tooltip"
                                            data-placement="top" title="Delete">
                                            <i class="fa fa-trash fa-sm"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <h1>Data Kosong</h1>
                        @endforelse
                </table>
            </div>
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection

@push('scripts')
    <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#kelas").DataTable();
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.btn-hapus').click(function(event) {
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: `Yakin Bakal Hapus Data ini?`,
                    text: "JIka Anda Yakin Data Pasti Bakal Hilang..!!!",
                    icon: "warning",
                    buttons: ["Tidak", "Yakin Lah.."],
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
@endpush
