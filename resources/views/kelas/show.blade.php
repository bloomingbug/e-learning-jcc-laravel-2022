@extends('adminlte.master')

@section('title')
    | Detail Kelas
@endsection

@section('judulutama')
    Daftar Kelas
@endsection

@section('judul')
    Daftar Kelas
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th scope="col">ID</th>
                        <td scope="col">: {{ $kelas->id }}</td>
                    </tr>
                    <tr>
                        <th scope="col">Nama Lengkap</th>
                        <td scope="col">: {{ $kelas->kode_kelas }}</td>
                    </tr>
                    <tr>
                        <th scope="col">Nama Kelas</th>
                        <td scope="col">: {{ $kelas->nama_kelas }}</td>
                    </tr>
                </tbody>
            </table>

            <table id="data-tables" class="table table-bordered table-hover mt-3">
                <thead>
                    <tr>
                        <th scope="col">no</th>
                        <th scope="col">NIS</th>
                        <th scope="col">Nama Lengkap</th>
                        <th scope="col">Tanggal Lahir</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Nomor HP</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($kelas->siswa as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $item->nis }}</td>
                            <td>{{ $item->nama_lengkap }}</td>
                            <td>{{ $item->tgl_lahir }}</td>
                            <td>{{ $item->alamat }}</td>
                            <td>{{ $item->no_hp }}</td>
                            <td>
                                <div class="d-flex flex-nowrap flex-column flex-md-row justify-center">
                                    <a href="/siswa/{{ $item->id }}" class="btn btn-info btn-sm m-1">
                                        <i class="fa fa-info-circle" aria-hidden="true">
                                        </i>
                                    </a>
                                    <a href="/siswa/{{ $item->id }}/edit" class="btn btn-warning btn-sm m-1">
                                        <i class="fa fa-edit" aria-hidden="true">
                                        </i>
                                    </a>
                                    <form action="/siswa/{{ $item->id }}" method="post" class="m-1 p-0">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm mx-auto w-100">
                                            <i class="fa fa-trash fa-sm"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>

                    @empty
                        <p>Data siswa Kosong!</p>
                    @endforelse
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            <a href="/kelas" class="btn btn-secondary btn-md">Kembali</a>
        </div>
    </div>
@endsection
