@extends('adminlte.master')

@section('title')
    | Edit Kelas
@endsection

@section('judulutama')
    Edit Kelas
@endsection

@section('judul')
    Form Edit Kelas
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="/kelas/{{ $kelas->id }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="nama">Kode Kelas</label>
                    <input type="text" class="form-control" id="nama" name="kode_kelas" value="{{ $kelas->kode_kelas }}">
                </div>
                @error('kode_kelas')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="nama">Nama Kelas</label>
                    <input type="text" class="form-control" id="nama" name="nama_kelas" value="{{ $kelas->nama_kelas }}">
                </div>
                @error('nama_kelas')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <a href="/kelas" class="btn btn-secondary btn-md">Kembali</a>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
