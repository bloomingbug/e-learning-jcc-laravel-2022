@extends('adminlte.master')

@section('title')
    | Tambah Kelas
@endsection

@section('judul')
    Tambah Data Kelas
@endsection

@section('link')
    @for ($i = 1; $i <= count(Request::segments()); $i++)
        @if ($i == count(Request::segments()))
            <li class="breadcrumb-item active">
                {{ ucfirst(trans(Request::segment($i))) }}
            </li>
        @else
            <li class="breadcrumb-item">
                <a href="{{ url('/' . Request::segment($i)) }}">{{ ucfirst(trans(Request::segment($i))) }}</a>
            </li>
        @endif
    @endfor
@endsection

@section('content')
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Kelas</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <!-- form start -->
                <form action="/kelas" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="kode_kelas">Kode Kelas</label>
                        <input type="text" class="form-control" name="kode_kelas" id="kode_kelas"
                            placeholder="Enter Kode Mata Pelajaran Ex. MTK011">
                    </div>
                    @error('kode_kelas')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <label for="nama_kelas">Nama Kelas</label>
                        <input type="text" class="form-control" name="nama_kelas" id="nama_kelas"
                            placeholder="Enter Nama Mata Pelajaran Ex. Matematika">
                    </div>
                    @error('nama_kelas')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <a href="/kelas/{{ Request::segment(2) }}" class="btn btn-warning"><i class="fa fa-arrow-left"
                            aria-hidden="true"></i> Kembali</a>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i>
                        Simpan
                    </button>
                </form>
            </div>
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection
