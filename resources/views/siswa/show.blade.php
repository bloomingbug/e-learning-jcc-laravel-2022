@extends('adminlte.master')

@section('title')
    | Detail Siswa
@endsection

@section('judulutama')
    Detail of {{ $siswa->nama_lengkap }}
@endsection

@section('judul')
    {{ $siswa->nama_lengkap }}
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th scope="col">NIS</th>
                        <td scope="col">{{ $siswa->nis }}</td>
                    </tr>
                    <tr>
                        <th scope="col">Nama Lengkap</th>
                        <td scope="col">{{ $siswa->nama_lengkap }}</td>
                    </tr>
                    <tr>
                        <th scope="col">Tanggal Lahir</th>
                        <td scope="col">{{ $siswa->tgl_lahir }}</td>
                    </tr>
                    <tr>
                        <th scope="col">E-Mail</th>
                        <td scope="col">{{ $siswa->user->email }}</td>
                    </tr>
                    <tr>
                        <th scope="col">Nomor Handphone</th>
                        <td scope="col">{{ $siswa->no_hp }}</td>
                    </tr>
                    <tr>
                        <th scope="col">Alamat</th>
                        <td scope="col">{{ $siswa->alamat }}</td>
                    </tr>
                    <tr>
                        <th scope="col">Role</th>
                        <td scope="col">{{ $siswa->user->role->nama }}</td>
                    </tr>
                    <tr>
                        <th scope="col">Kelas</th>
                        <td scope="col">{{ $siswa->kelas[0]->nama_kelas }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            <a href="/siswa" class="btn btn-secondary btn-md">Kembali</a>
        </div>
    </div>
@endsection
