@extends('adminlte.master')

@section('title')
    | Edit Siswa
@endsection

@section('link')
    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection

@section('judul')
    Edit of {{ $siswa->nama_lengkap }}
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="/siswa/{{ $siswa->id }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>NIS</label>
                    <input type="number" class="form-control" name="nis" min="0" value="{{ $siswa->nis }}">
                </div>
                @error('nis')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>User Name</label>
                    <input type="text" class="form-control" name="name" value="{{ $siswa->user->name }}">
                </div>
                @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>e-mail</label>
                    <input type="text" class="form-control" name="email" value="{{ $siswa->user->email }}">
                </div>
                @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama_lengkap" value="{{ $siswa->nama_lengkap }}">
                </div>
                @error('nama_lengkap')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <input type="date" class="form-control" name="tgl_lahir" value="{{ $siswa->tgl_lahir }}">
                </div>
                @error('tgl_lahir')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Nomor Handphone</label>
                    <input type="text" class="form-control" name="no_hp" value="{{ $siswa->no_hp }}">
                </div>
                @error('no_hp')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Alamat</label>
                    <textarea name="alamat" class="form-control">{{ $siswa->alamat }}</textarea>
                </div>
                @error('alamat')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="kelas_id">Kelas</label>
                    <select name="kelas_id" id="kelas_id" class="form-control">
                        <option value="" disabled>---Pilih Kelas---</option>
                        @foreach ($kelas as $item)
                            @if ($item->id === $siswa->kelas[0]->id)
                                <option value="{{ $item->id }}" selected>{{ $item->nama_kelas }}</option>
                            @else
                                <option value="{{ $item->id }}">{{ $item->nama_kelas }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                @error('kelas_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <a href="/siswa" class="btn btn-secondary btn-md">Kembali</a>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
