@extends('adminlte.master')

@section('title')
    | Tambah Siswa
@endsection

@section('judulutama')
    Tambah Siswa
@endsection

@section('judul')
    Form Tambah Siswa
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="/siswa" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="card border border-secondary card-secondary">
                            <div class="card-header">
                                <h5 class="card-text">Pribadi</h5>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>NIS</label>
                                    <input type="number" class="form-control" name="nis" min="0"
                                        value="{{ old('nis') }}">
                                </div>
                                @error('nis')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group">
                                    <label>Nama Lengkap</label>
                                    <input type="text" class="form-control" name="nama_lengkap"
                                        value="{{ old('nama_lengkap') }}">
                                </div>
                                @error('nama_lengkap')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <input type="date" class="form-control" name="tgl_lahir"
                                        value="{{ old('tgl_lahir') }}">
                                </div>
                                @error('tgl_lahir')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group">
                                    <label>Nomor Handphone</label>
                                    <input type="text" class="form-control" name="no_hp" value="{{ old('no_hp') }}">
                                </div>
                                @error('no_hp')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea name="alamat" class="form-control" rows="5.9">{{ old('alamat') }}</textarea>
                                </div>
                                @error('alamat')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card border border-secondary card-secondary">
                            <div class="card-header">
                                <h5 class="card-text">User</h5>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Username</label>
                                    <input type="text" class="form-control" id="name" name="name"
                                        value="{{ old('name') }}">
                                </div>
                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group">
                                    <label for="email">E-Mail</label>
                                    <input type="text" class="form-control" id="email" name="email"
                                        value="{{ old('email') }}">
                                </div>
                                @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="password" name="password">
                                </div>
                                @error('password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group">
                                    <label for="role_id">Role</label>
                                    <select name="role_id" id="role_id" class="form-control">
                                        <option value="">---Pilih Role---</option>
                                        @foreach ($role as $item)
                                            <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @error('role_id')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="card border border-secondary card-secondary">
                            <div class="card-header">
                                <h5 class="card-text">Kelas</h5>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="kelas_id">Kelas</label>
                                    <select name="kelas_id" id="kelas_id" class="form-control">
                                        <option value="" disabled>---Pilih Kelas---</option>
                                        @foreach ($kelas as $item)
                                            <option value="{{ $item->id }}">{{ $item->nama_kelas }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @error('kelas_id')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>
                    </div>
                </div>
                <a href="/siswa" class="btn btn-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i>
                    Simpan</button>
            </form>
        </div>
    @endsection
