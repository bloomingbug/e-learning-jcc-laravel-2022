@extends('adminlte.master')

@section('title')
    | Siswa
@endsection

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css" />
@endpush

@section('title')
    | Daftar Siswa
@endsection

@section('link')
    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
    <li class="breadcrumb-item active">Siswa</li>
@endsection

@section('judul')
    Daftar Siswa
@endsection

@section('content')
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <a href="/siswa/create" class="btn btn-primary btn-md"><i class="fa-solid fa-file-plus"></i> Tambah Siswa</a>
            <a href="/export/siswa" class="btn btn-info btn-md ml-auto"><i class="fa-solid fa-file-plus"></i> Export</a>
        </div>
        <div class="card-body">
            <table id="data-tables" class="table table-bordered table-hover mt-3">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">NIS</th>
                        <th scope="col">Nama Lengkap</th>
                        <th scope="col">Tanggal Lahir</th>
                        <th scope="col">Nomor HP</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($siswa as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $item->nis }}</td>
                            <td>{{ $item->nama_lengkap }}</td>
                            <td>{{ $item->tgl_lahir }}</td>
                            <td>{{ $item->no_hp }}</td>
                            <td>{{ Str::limit($item->alamat, 20) }}</td>
                            <td>
                                <div class="d-flex flex-nowrap flex-column flex-md-row justify-center">
                                    <a href="/siswa/{{ $item->id }}" class="btn btn-info btn-sm m-1">
                                        <i class="fa fa-info-circle" aria-hidden="true">
                                        </i>
                                    </a>
                                    <a href="/siswa/{{ $item->id }}/edit" class="btn btn-warning btn-sm m-1">
                                        <i class="fa fa-edit" aria-hidden="true">
                                        </i>
                                    </a>
                                    <form action="/siswa/{{ $item->id }}" method="post" class="m-1 p-0">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm mx-auto w-100">
                                            <i class="fa fa-trash fa-sm"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>

                    @empty
                        <p>Data Masih Kosong!</p>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#data-tables").DataTable();
        });
    </script>
@endpush
