@extends('adminlte.master')

@section('title')
    | Profile
@endsection

@section('link')
    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection

@section('judul')
    Edit Profile of {{ Auth::user()->name }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <a href="/dashboard" class="btn btn-secondary btn-md mb-2">Kembali</a>
        </div>
        <div class="card-body">
            <form action="/profile-guru/{{ $profile->id }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>NIK</label>
                    <input type="number" class="form-control" name="nik" value="{{ $profile->nik }}" disabled>
                </div>
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" class="form-control" name="nama" value="{{ Auth::user()->name }}" disabled>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" value="{{ Auth::user()->email }}" disabled>
                </div>
                <div class="form-group">
                    <label>Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama" value="{{ $profile->nama }}">
                </div>
                @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <input type="date" class="form-control" name="tgl_lahir" value="{{ $profile->tgl_lahir }}">
                </div>
                @error('tgl_lahir')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="form-group">
                    <label>Nomor Handphone</label>
                    <input type="text" class="form-control" name="no_hp" value="{{ $profile->no_hp }}">
                </div>
                @error('no_hp')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="form-group">
                    <label>Alamat</label>
                    <textarea name="alamat" class="form-control">{{ $profile->alamat }}</textarea>
                </div>
                @error('alamat')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
