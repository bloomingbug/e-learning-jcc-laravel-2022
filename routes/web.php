<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\MapelController;

// Ngatur Login/Register
Auth::routes();

Route::middleware('admin')->group(function(){
  Route::resource('mapel', "MapelController");
  // Route CRUD User
  Route::resource('user', 'UserController');
  
  // CRUD Siswa
  Route::resource('siswa', 'SiswaController');

  // CRUD Guru
  Route::resource('guru', 'GuruController');

  // CRUD Kelas
  Route::resource('kelas', 'KelasController');
  
  // CRUD Role
  Route::resource('role', 'RoleController');

  // Route CRUD for ContentMapelController use Model ContentMapel on table content_mapel
  Route::get('/mapel/{id}/content/create', "ContentMapelController@create");
  Route::post('/mapel/content', "ContentMapelController@store");
  Route::get('/mapel/content/edit/{id}', "ContentMapelController@edit");
  Route::put('/content/{id}', "ContentMapelController@update");
  Route::delete('/mapel/content/{id}', "ContentMapelController@hapus");
});

Route::middleware('guru')->group(function(){
  Route::resource('mapel', "MapelController")->only('index', 'show');
  Route::resource('profile-guru', 'ProfileGuruController')->only('index', 'update');
  
  // Route CRUD for ContentMapelController use Model ContentMapel on table content_mapel
  Route::get('/mapel/{id}/content/create', "ContentMapelController@create");
  Route::post('/mapel/content', "ContentMapelController@store");
  Route::get('/mapel/content/edit/{id}', "ContentMapelController@edit");
  Route::put('/content/{id}', "ContentMapelController@update");
  Route::delete('/mapel/content/{id}', "ContentMapelController@hapus");
});

Route::middleware('siswa')->group(function(){
  Route::get('mapelsiswa', "MapelSiswaController@index");
  Route::get('mapelsiswa/{id}', "MapelSiswaController@show");
  Route::resource('profile-siswa', 'ProfileSiswaController')->only('index', 'update');
});


// Mengalihkan kehalaman View
Route::get('/', 'HomeController@index')->name('home');
Route::get('/index', 'HomeController@index');
Route::get('/dashboard', 'HomeController@index');

// Export Table Siswa
Route::get('/export/siswa', 'ExcelController@export_siswa');

// Export Table Guru
Route::get('/export/guru', 'ExcelController@export_guru');

// Export Table Kelas
Route::get('/export/kelas', 'ExcelController@export_kelas');

// Export Table Mapel
Route::get('/export/mapel', 'ExcelController@export_mapel');