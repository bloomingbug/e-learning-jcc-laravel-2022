# Final Project JCC Backend End Web Laravel

## Kelompok 27

## 👯 Anggota Kelompok

1. Ahmad Jumadi (@Jmd611)
2. Rifky Maulana (@rifky_maaul)
3. Tarmuji (@tarmuji22)\*\*

## 💬 Tema Proyek

[E-Learning](http://e-lena.jum.my.id/)

## 💬 Deskripsi Proyek

**Elearning** merupakan sistem manajemen pembelajaran daring yang bisa mempermudah sistem belajar online. Elearning dibangun dengan framework PHP Laravel dan tujuan utama pembuatannya yaitu untuk memenuhi final project Jabar Coding Camp batch 2.

Dalam **Elearning** ada tiga jenis role/jabatan yaitu admin, guru, dan siswa yang masing masing dari role tersebut memiliki autorisasinya masing masing.

## 📄 ERD

  <img src="public/erd/erd.png" alt="erd" />

## 🌱 Library

-   Library **DataTables, SweetAlert, Laravel Excel, ChartJS, MomentJS**
-   Template **AdminLTE**

## 🌱 UI Framework

-   CSS **Bootstrap**

## ⚡ Link Video

-   Link Deploy : [JUM.MY.ID](http://e-lena.jum.my.id/)
-   Link Video Demo : [YOUTUBE](https://youtu.be/a0URneAaYpc)

## 🌱 Languages and Tools:

<p align="left"> <a href="https://getbootstrap.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/bootstrap/bootstrap-plain-wordmark.svg" alt="bootstrap" width="40" height="40"/> </a> <a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a> <a href="https://laravel.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/laravel/laravel-plain-wordmark.svg" alt="laravel" width="40" height="40"/> </a> <a href="https://www.mysql.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="40" height="40"/> </a> <a href="https://www.php.net" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/php/php-original.svg" alt="php" width="40" height="40"/> </a> </p>
